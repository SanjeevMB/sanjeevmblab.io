---
title: path
date: 2023-03-15 15:24:13
tags: [path]
---
# path

1. The path to a specified file consists of one or more segments of strings, separated by a special character (a backslash or forward slash), with each segment usually being a directory name or file name.
2. The `path` module is a built-in module in JavaScript that provides methods for working with file and directory paths. It is used to manipulate file paths and directory paths in a platform-independent way.
3. The `path` module can be used in both Node.js and the browser.
4. The `path` module provides a set of methods to manipulate paths.
    1. `path.join()`
    2. `path.resolve()`
    3. `path.dirname()`
    4. `path.extname()`
    5. `path.isAbsolute()`
   
5. The `path` module can be accessed by using the below syntax.
~~~js
const path = require('path');
~~~
***
## path.join()
***

1. The `path.join()` method joins all given path segments together using the platform-specific separator as a delimiter, then normalizes the resulting path.

2. Zero-length path segments are ignored. If the joined path string is a zero-length string then '.' will be returned, representing the current working directory.

~~~js
const path = require('path');

let absPath = path.join('/user', 'sanjeev', 'mountblue', 'javascript-blog.md');

console.log(absPath);

// Extected output: '/user/sanjeev/mountblue/javascript-blog.md'

let mbPath = path.join('mountblue', '', 'bar', '..', 'test.cjs');

console.log(mbPath);

// Expected output: '/mountblue/test.cjs'

let wrongPath = path.join('/random', {}, 'file.txt');

console.log(wrongPath);

// TypeError [ERR_INVALID_ARG_TYPE]: The "path" argument must be of type string. Received an instance of Object'

~~~
***

## path.resolve()
***

1. The `path.resolve()` method resolves a sequence of paths or path segments into an absolute path.

2. The given sequence of paths is processed from right to left, with each subsequent path prepended until an absolute path is constructed. For instance, given the sequence of path segments: /foo, /bar, baz, calling path.resolve('/foo', '/bar', 'baz') would return /bar/baz because 'baz' is not an absolute path but '/bar' + '/' + 'baz' is.

3. If, after processing all given path segments, an absolute path has not yet been generated, the current working directory is used.

4. The resulting path is normalized and trailing slashes are removed unless the path is resolved to the root directory.

5. Zero-length path segments (empty strings) are ignored.

6. If no path segments are passed, path.resolve() will return the absolute path of the current working directory.

~~~js
const path = require('path');

let path1 = path.resolve('/myName/subDirectory/sanjeev/', './Mountblue');

console.log(path1);

// Expected output: '/myName/subDirectory/sanjeev/Mountblue'

let path2 = path.resolve('/sanjeev/file', '/ramesh/file/', '/sachin/file/');

// Expected output: '/sachin/file'

let path3 = path.resolve('hello', 'files/png/', '../gif/image.gif');

console.log(path3);

// Expected output: '/home/sanjeev/Mountblue/Rough-work/hello/files/gif/image.gif'
~~~