---
title: promise
date: 2023-03-14 15:53:44
tags: [promise]
---
# promise

### A promise in JavaScript is just like a promise in real life. The promise is a returned object and it has two results either the promise has been completed or it has failed. In Java Script, a promise function gets two parameters which are resolved and reject.

A Promise is in one of these states

1. pending, the initial state neither fulfilled nor rejected.
2. fulfilled, meaning that the operation was completed successfully.
3. rejected, meaning that the operation failed.

## Syntax of creating a promise

~~~js
let promise = new Promise((resolve, reject) => {

    let task = true;

    if(task){

        resolve('Task completed');

    }else{

        reject('Task failed');

    }

});

promise.then((data) => {

    console.log(data.toString());

    // Extected output : 'Task completed'

});
~~~

## 1. Promise.all()
***

The `Promise.all()` is a method that iterates over more Promises. This returned promise when all of the input's promises are fulfilled. (including when an empty iterable is passed), with an array of fulfillment values. It rejects when any of the input's promises rejects, with this first rejection reason.

~~~js
let promise1 = new Promise((resolve, reject) => {

    if(true){

        resolve('Successfull 1');

    }else{

        reject('Failed');

    }

});

let promise2 = new Promise((resolve, reject) => {

    if(true){

        resolve('Successfull 2');

    }else{

        reject('Failed');

    }

});
let promise3 = new Promise((resolve, reject) => {
    if(true) {

        resolve('Successfull 3');

    }else{

        reject('Failed');

    }

});

Promise.all([promise1, promise2, promise3]).then((data) => {

  console.log(data.toString());

  // Extected output : 'Successfull 1', 'Successfull 2', 'Successfull 3'

});
~~~

## 2. Promise.allSettled()
***
The `Promise.allSettled()` method takes an array of promises as input and returns a single Promise. This returned promise fulfills when all of the input's promises settle (including when an empty iterable is passed), with an array of objects that describe the outcome of each promise. `Promise.allSettled()` is typically used when you have multiple asynchronous tasks that are not dependent on one another to complete successfully, or you'd always like to know the result of each promise.

~~~js
let promise1 = Promise.resolve('Successful');
let promise2 = new Promise((resolve, reject) => {

    resolve('Succlessful');

});

let promises = [promise1, promise2];

Promise.allSettled(promises).
  then((results) => {results.forEach(result) => console.log(result)});

~~~

## 3. Promise.any()
***
The `Promise.any()` method takes an array of promises as input and returns a single Promise. This returned promise fulfills when any of the input's promises are fulfilled, with this first fulfillment value. It rejects when all of the input's promises reject (including when an empty iterable is passed), with an Error.
This method is useful for returning the first promise that was fulfilled. so it does not wait for the other promises to be complete once it finds one fulfilled.

~~~js
let promise1 = new Promise((resolve, reject) => {
  reject("Failed");
});

let promose2 = new Promise((resolve, reject) => {

  reject("Successfull first");

});

let promise3 = new Promise((resolve, reject) => {

  resolve("Successfull second");

});

Promise.any([promise1, promise2, promise3]).then((data) => {

  console.log(data.toString());

  // Expected output : 'Successfull first'

});
~~~

## 4. Promise.race()
***
The `Promise.race()` method takes an array of promises as input and returns a single Promise. This returned promise settles with the eventual state of the first promise that settles. It's useful when you want the first async task to complete, but do not care about its eventual state.

~~~js
let promise1 = new Promise((resolve, reject) => {

  setTimeout(resolve, 2000, 'First');

});

let promise2 = new Promise((resolve, reject) => {

  setTimeout(resolve, 1000, 'Second');

});

Promise.race([promise1, promise2]).then((data) => {

  console.log(data.toString());

  // Extected output : 'Second'

});
~~~